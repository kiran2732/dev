<?php

require_once('./TCPDF/tcpdf.php');

require_once "db.php";

$res = $builder->select('*')
       ->from('employees')
       ->where('status = ?')
       ->setParameter(0,1)
       ->fetchAllAssociative();

$content = '
    <h1 align="center">Employee Details</h1>
    <table border="1" cellspacing="3" cellpadding="4">
    <thead>
      <tr>
        <th><h3>ID</h3></th>
        <th><h3>First Name</h3></th>
        <th><h3>Last Name</h3></th>
        <th><h3>Email</h3></th>
        <th><h3>Address</h3></th>
      </tr>
    </thead>
    <tbody>
';

foreach($res as $k=>$v) {
    $content .= '
        <tr>
            <th>'.$v["id"].'</th>
            <td>'.$v["first_name"].'</td>
            <td>'.$v["last_name"].'</td>
            <td>'.$v["email"].'</td>
            <td>'.$v["address"].'</td>
        </tr>
    ';
}

$content .= '
    </tbody>
    <table>
';

$conn = null;

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('Employee Details');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// set font
$pdf->SetFont('times', '', 12);

// add a page
$pdf->AddPage();

// print a block of text using Write()
$pdf->writeHTML($content);

//Close and output PDF document
$pdf->Output('employee_details.pdf', 'I');