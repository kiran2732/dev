<?php
require __DIR__ . '/vendor/autoload.php';

use \Firebase\JWT\JWT;

$accessKey = 'ssOn5qSME8x0T4rzQw8dbh2KRbs0DX8zi6QtSnqmuVU43a8Rqmxugalu01AT';
$environmentId = 'K0bNE0ZVUOsMLoQpcbXx';

$payload = array(
    'aud' => $environmentId,
    'iat' => time(),
    'sub' => 'user-123',
    // 'user' => array(
    //     'email' => 'joe.doe@example.com',
    //     'name' => 'Joe Doe'
    // ),
    // 'auth' => array(
    //     'collaboration' => array(
    //         '*' => array(
    //             'role' => 'writer',
    //         )
    //     )
    // )
);

$jwt = JWT::encode($payload, $accessKey, 'HS256');

// Here we are printing the token to the console. In a real usage scenario
// it should be returned in an HTTP response of the token endpoint.
echo $jwt;
?>
