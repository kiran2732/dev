<?php

require_once "db.php";

$res = $builder->select('*')
       ->from('state')
       ->orderBy('state', 'ASC')
       ->fetchAllAssociative();    

echo json_encode($res);
