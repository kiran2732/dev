<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP & MySQL AJAX example Using jQuery</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css"/>
</head>
<body>
    <div class="container">
        <h1>
            <a href="https://dev.to/codeanddeploy/php-mysql-ajax-example-using-jquery-35g4">
                PHP & MySQL AJAX example Using jQuery
            </a>
        </h1>
        <br><br>
        <div class="row">
            <div class="col-md-5">
                <h3>Add New Employee</h3>

                <div id="formDisplay"></div>

            </div>

            <div class="col-md-7">
                <h3>List of Employees</h3>
                <div id="employees-list"></div>
            </div>
            
        </div>
        <br>
        <a href="mail.php" class="btn btn-info"> send mail</a>

    </div>  
    
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script></script> 
    <!-- PNotify -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/attempt-to-update-packagist/pnotify.min.css" integrity="sha512-/odx7DzBB1VWp8CF129ic9JIUkR8Lv32iJcGeugeJ6JyeYwdUKtlYAs579Bg1EOtKnuFPyMYF/kFkc06YlBXLw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/attempt-to-update-packagist/pnotify.brighttheme.min.css" integrity="sha512-yCFX/FepnMle8kYRBT7g101YTrak/0DmXufVerzyoeUNXLQxUn4egRC/iiXYG+nmH2nWnr6voQPusvDTQFEaTw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/attempt-to-update-packagist/pnotify.buttons.min.css" integrity="sha512-swjSypf3sg74W5zzlIwlQoz1pvVvvMUpCpqPQGiKmHLUPARPzxz5kV8Z6eMX5ZhXD/tRVnZDhBk4YItvS+GDXw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/attempt-to-update-packagist/pnotify.min.js" integrity="sha512-983oc74ODyMuWXo39bH4kvq7hcVZX4O19EgEq3vbK4BRmMY7nxP3CFvjjbW4lu2tWfR7HMSHyKVQ44atgjl5qA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/attempt-to-update-packagist/pnotify.buttons.min.js" integrity="sha512-co9eQf9iBrZ/C9x6P5eMTN2J+U/RHzcY9AymqhM8AgNp7s83z9RndzfsUKSP7O+6CJb1JUf1fOkmjBb4+YT9qQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/attempt-to-update-packagist/pnotify.animate.min.js" integrity="sha512-94O16tExnopNEzgFyGfOoBqpapZz/zIvwR1XkEarweIWD+Srfb0I9ADMKyrMKFEXFFhCS+Qq313epIg3fUzkWg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- SELECT2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <!-- Page Script -->
    <script src="scripts.js"></script>
</body>
</html>