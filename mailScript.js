$(document).ready(function () {

  DecoupledEditor.create( document.querySelector( '.document-editor__editable' ), {
      cloudServices: {
        tokenUrl: 'token.php',
        uploadUrl: 'https://89320.cke-cs.com/easyimage/upload/'
      }
    } )
    .then( editor => {
      const toolbarContainer = document.querySelector( '.document-editor__toolbar' );

        toolbarContainer.appendChild( editor.ui.view.toolbar.element );

        window.editor = editor;
        console.log(editor.getData())

        // $('.document-editor__editable').on('DOMSubtreeModified', function(){
        //   $("#emailbody").html(editor.getData());
        // })
      
      $('.document-editor__editable').on('keyup', function() {
        $(this).html(editor.getData());
          $(this).on('focusout', function(){
            $("#emailbody").html(editor.getData());
          })
        console.log(editor.getData()) 
      })
    } )
    .catch( error => {
        console.error( error );
    } );

    email();
  });
  
  
  function resetEmailForm() {
    $("#emailForm")[0].reset();
    $(".ck-content *").html("");
  }
  
  function email() {
    $("#sendEmail").on("click", function () {
      $.ajax({
        type: "POST",
        url: $("#emailForm").attr("action"),
        data: $("#emailForm").serializeArray(),
        beforeSend: function () {
          $("#sendEmail").attr("disabled", true).html("Sending...");
        },
        success: function (response) {
          $("#sendEmail").attr("disabled", false).html("Send");
  
          new PNotify({
            text: "Mail has been sent successfully.",
            type: "success",
            buttons: {
              sticker: false,
            },
            animate_speed: "fast",
          });
  
          //reset form
          resetEmailForm();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          new PNotify({
            text: "Error while sending email!",
            type: "error",
            buttons: {
              sticker: false,
            },
            animate_speed: "fast",
          });
        },
      });
    });
  }