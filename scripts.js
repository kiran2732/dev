function all() {
  var html = "";
  html +=
    '<table id="example" class="table-striped table-bordered" style="width:100%">';
  html +=
    "<thead><tr><th>ID</th><th>First Name</th><th>Last Name</th><th>Email</th><th>State</th><th>Address</th><th>Created On</th><th>Action</th></tr></thead><tbody>";

  html += "</tbody></table>";
  // Insert the HTML Template and display all employee records
  $("#employees-list").html(html);

  $("#example").DataTable({
    ajax: {
      url: "all.php",
      dataSrc: function (json) {
        for (let i = 0; i < json.length; i++) {
          json[i]["button"] =
            "<button class='btn btn-danger delete' onclick='deleteEmployee()' id='" +
            json[i]["id"] +
            "'>Delete</button><button class='btn btn-info edit' onClick='editEmployeeDetails(" +
            json[i]["id"] +
            ")'>Edit</button>";
        } 
        return json;
      },
    },
    columns: [
      { data: "id" },
      { data: "first_name" },
      { data: "last_name" },
      { data: "email" },
      { data: "state" },
      { data: "address" },
      { data: "created_on" },
      { data: "button" },
    ],
  });
}

function state(s = "") {
  $.ajax({
    type: "GET",
    url: "getState.php",
    success: function (response) {
      // parse the json result
      response = JSON.parse(response);

      var html = "";

      // check if there is available records
      if (response.length) {
        // Loop the parsed JSON
        html += "<option value='" + s + "'>" + s + "</option>";
        $.each(response, function (key, value) {
          // Our employee list template
          html +=
            "<option value='" + value.state + "'>" + value.state + "</option>";
        });
      } else {
        html += '<div class="alert alert-warning">';
        html += "No records found!";
        html += "</div>";
      }
      // Insert the HTML Template and display all employee records
      $("#state").html(html);
    },
  });
}

function selectState() {
  $("#state").select2({
    placeholder: "Select a state",
    allowClear: true,
    // theme: "classic",
  });
}

function submitForm() {
  $("#btnSubmit").on("click", function () {
    $.ajax({
      type: "POST",
      url: $("#form").attr("action"),
      data: $("#form").serializeArray(),
      beforeSend: function () {
        $("#btnSubmit").attr("disabled", true).html("Processing...");
      },
      success: function (response) {
        $("#btnSubmit").attr("disabled", false).html("Submit");
        // Reload lists of employees
        all();

        new PNotify({
          text: response,
          type: "success",
          buttons: {
            sticker: false,
          },
          animate_speed: "fast",
        });

        //reset form
        resetForm();
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        new PNotify({
          text: "Error while submitting form!",
          type: "error",
          buttons: {
            sticker: false,
          },
          animate_speed: "fast",
        });
      },
    });
  });
}

function updateDetails() {
  $("#saveEmployeeDetail").on("click", function () {
    $.ajax({
      type: "POST",
      url: $("#form").attr("action"),
      data: $("#form").serializeArray(),
      beforeSend: function () {
        $("#saveEmployeeDetail").attr("disabled", true).html("Processing...");
      },
      success: function (response) {
        $("#saveEmployeeDetail").attr("disabled", false).html("Save");
        // Reload lists of employees
        all();
        displayForm();

        new PNotify({
          text: response,
          type: "success",
          buttons: {
            sticker: false,
          },
          animate_speed: "fast",
        });
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        new PNotify({
          text: "Error while submitting form!",
          type: "error",
          buttons: {
            sticker: false,
          },
          animate_speed: "fast",
        });
      },
    });
  });
}

function editEmployeeDetails(id) {
  $.ajax({
    type: "GET",
    url: "getOne.php?id=" + id,
    success: function (response) {
      response = JSON.parse(response);
      html =
        '<form action="update.php" id="form"> <input type="hidden" name="id" id="id" value="' +
        response.id +
        '"><div class="form-group"><label for="email">Email</label><input type="text" name="email" id="email" value="' +
        response.email +
        '" class="form-control"></div>';
      html +=
        '<div class="form-group"><label for="first_name">First Name</label><input type="text" name="first_name" id="first_name" value="' +
        response.first_name +
        '" class="form-control"></div>';
      html +=
        '<div class="form-group"><label for="last_name">Last Name</label><input type="text" name="last_name" id="last_name" value="' +
        response.last_name +
        '" class="form-control"></div>';
      html +=
        '<div class="form-group"><label for="state">Select State:</label><select class="form-control" name="state" id="state"></select> </div>';
      html +=
        '<div class="form-group"><label for="address">Address</label><br><textarea type="text" name="address" id="address" class="address" rows="3" cols="35">' +
        response.address +
        "</textarea></div>";
      html +=
        '<button type="button" class="btn btn-primary mx-3" id="saveEmployeeDetail">Save</button>';
      html +=
        '<button type="button" class="btn btn-warning" onclick="displayForm()">Add New Employee</button> </form>';

      $("#formDisplay").html(html);
      state(response.state);
      $("#state").select2();
      updateDetails();
    },
  });
}

function deleteEmployee() {
  // $(".delete").click(function () {
  $id = $(".delete").attr("id");
  $.ajax({
    type: "POST",
    url: "delete.php?id=" + $id,
    success: function (response) {
      all();
      // pNotifyTicketAlert(response, "error");
      new PNotify({
        text: response,
        type: "info",
        buttons: {
          sticker: false,
        },
        animate_speed: "fast",
      });
    },
  });
  // });
}

function resetForm() {
  $("#form")[0].reset();
  $("#state").select2({
    placeholder: "Select a state",
  });
}

function displayForm() {
  html =
    '<form action="save.php" id="form"> <div class="form-group"><label for="email">Email</label><input type="text" name="email" id="email" class="form-control"></div><div class="form-group"><label for="first_name">First Name</label><input type="text" name="first_name" id="first_name" class="form-control"></div><div class="form-group"><label for="last_name">Last Name</label><input type="text" name="last_name" id="last_name" class="form-control"></div><div class="form-group"><label for="state">Select State:</label><select class="form-control" name="state" id="state"></select> </div><div class="form-group"><label for="address">Address</label><br><textarea type="text" name="address" id="address" class="address" rows="3" cols="35"></textarea></div><button type="button" class="btn btn-primary" id="btnSubmit">Submit</button><br><br><a href="tcpdf.php" class="btn btn-warning" target="_blank">Export to PDF</a></form>';

  $("#formDisplay").html(html);
  state();
  selectState();
  submitForm();
}

$(document).ready(function () {
  displayForm();
  // Get all employee records
  all();

  // Submit form using AJAX
  submitForm();

  state();
  selectState();
});