<?php

require_once "db.php";

$res = $builder->select('*')
       ->from('employees')
       ->where('status = ?')
       ->setParameter(0,1)
       ->fetchAllAssociative();

echo json_encode($res);
