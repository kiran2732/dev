<?php

$id = $_GET['id'];

$currentDateTime = date('Y-m-d H:i:s');

if($id != 0) {

    require_once "db.php";

    $res = $builder->update('employees')
           ->set('status',0)
           ->set('updated_on', '?')->setParameter(0, $currentDateTime)
           ->where('id = ?')
           ->setParameter(1,$id);
    
    if($res->executeQuery()) {
        echo "Employee has been deleted successfully.";
    } else {
        echo "Error while deleting employee!";
    }
    
} else {
    echo "Error!";
}