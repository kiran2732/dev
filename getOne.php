<?php

$id = $_GET['id'];

if($id != 0) {

    require_once "db.php";

    $res = $builder->Select('*')
    ->from('employees')
    ->where('id = ?')
    ->setParameter(0,$id)
    ->fetchAssociative();

    echo json_encode($res);
    
} else {
    echo "Error!";
}