<?php

require_once 'db.php';

$request = $_REQUEST;
$email = $request['email'];
$first_name = $request['first_name'];
$last_name = $request['last_name'];
$address = $request['address'];
$state = $request['state'];

$res = $builder->insert('employees')
       ->setValue('email', '?')
       ->setValue('first_name', '?')
       ->setValue('last_name', '?')
       ->setValue('address', '?')
       ->setValue('state', '?')
       ->setParameter(0, $email)
       ->setParameter(1, $first_name)
       ->setParameter(2, $last_name)
       ->setParameter(3, $address)
       ->setParameter(4, $state);

if($res->executeQuery()) {
    echo "Employee has been created successfully.";
} else {
    echo "Error while saving employee details!";
}