<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP & MySQL AJAX example Using jQuery</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="editor.css">
    <!-- CKEditor -->
    <script src="https://cdn.ckeditor.com/ckeditor5/34.0.0/decoupled-document/ckeditor.js"></script>
    <!-- <script src="https://cdn.ckeditor.com/ckeditor5/34.0.0/classic/ckeditor.js"></script> -->
</head>
<body>
    <div class="container">
        <h1>
            <a href="https://dev.to/codeanddeploy/php-mysql-ajax-example-using-jquery-35g4">
                PHP & MySQL AJAX example Using jQuery
            </a>
        </h1>
        <br><br>
        
        <form id="emailForm" action="sendMail.php">
        <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Enter name">
            </div>
            <div class="form-group">
                <label for="email">To:</label>
                <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="Enter email">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
                <label for="subject">Subject:</label>
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Enter Subject">
            </div>
            <div class="form-group">
                <label for="body">Compose:</label>   
                <div class="document-editor">
                    <div class="document-editor__toolbar"></div>
                    <div class="document-editor__editable-container">
                        <div class="document-editor__editable"></div>
                    </div>
                </div>
                <textarea name="emailbody" id="emailbody"></textarea>
            </div>
            <br>        
            <button type="submit" class="btn btn-primary" id='sendEmail'>Send</button>
            <br><br><br>    
        </form>
    </div>  
    </body>
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/attempt-to-update-packagist/pnotify.min.css" integrity="sha512-/odx7DzBB1VWp8CF129ic9JIUkR8Lv32iJcGeugeJ6JyeYwdUKtlYAs579Bg1EOtKnuFPyMYF/kFkc06YlBXLw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/attempt-to-update-packagist/pnotify.brighttheme.min.css" integrity="sha512-yCFX/FepnMle8kYRBT7g101YTrak/0DmXufVerzyoeUNXLQxUn4egRC/iiXYG+nmH2nWnr6voQPusvDTQFEaTw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/attempt-to-update-packagist/pnotify.buttons.min.css" integrity="sha512-swjSypf3sg74W5zzlIwlQoz1pvVvvMUpCpqPQGiKmHLUPARPzxz5kV8Z6eMX5ZhXD/tRVnZDhBk4YItvS+GDXw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/attempt-to-update-packagist/pnotify.min.js" integrity="sha512-983oc74ODyMuWXo39bH4kvq7hcVZX4O19EgEq3vbK4BRmMY7nxP3CFvjjbW4lu2tWfR7HMSHyKVQ44atgjl5qA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/attempt-to-update-packagist/pnotify.buttons.min.js" integrity="sha512-co9eQf9iBrZ/C9x6P5eMTN2J+U/RHzcY9AymqhM8AgNp7s83z9RndzfsUKSP7O+6CJb1JUf1fOkmjBb4+YT9qQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/attempt-to-update-packagist/pnotify.animate.min.js" integrity="sha512-94O16tExnopNEzgFyGfOoBqpapZz/zIvwR1XkEarweIWD+Srfb0I9ADMKyrMKFEXFFhCS+Qq313epIg3fUzkWg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- Page Script -->
    <script src="mailScript.js"></script>

</html>