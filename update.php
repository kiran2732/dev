<?php

require_once 'db.php';

$request = $_REQUEST;
$id = $request['id'];
$email = $request['email'];
$first_name = $request['first_name'];
$last_name = $request['last_name'];
$address = $request['address'];
$state = $request['state'];

$currentDateTime = date('Y-m-d H:i:s');

$res = $builder->update('employees')
       ->set('email', '?')->setParameter(0, $email)
       ->set('first_name', '?')->setParameter(1, $first_name)
       ->set('last_name', '?')->setParameter(2, $last_name)
       ->set('address', '?')->setParameter(3, $address)
       ->set('state', '?')->setParameter(4, $state)
       ->set('updated_on', '?')->setParameter(5, $currentDateTime)
       ->where('id = '.$id );

if($res->executeQuery()) {
    echo "Employee details has been updated successfully.";
} else {
    echo "Please change details to update!";
}